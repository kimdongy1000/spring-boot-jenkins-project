package com.example.demo.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.regex.Pattern;

@Configuration
public class CommonString {

    private static final String WINDOW_REGEX = "^windows.*$";
    private static final String LINUX_REGEX = "^linux.*$";

    @Bean
    public String returnOsSystemName(){

        String os_name = System.getProperty("os.name").toString().toLowerCase();

        if(Pattern.matches(WINDOW_REGEX , os_name)){

            return "windows";

        }else if(Pattern.matches(LINUX_REGEX , os_name)){

            return "linux";
        }

        return "windows"; // 기본 os
    }
}
