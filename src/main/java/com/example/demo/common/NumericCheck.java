package com.example.demo.common;

import org.springframework.stereotype.Component;

/**
 *
 * 문자열 입력시 숫자인지 아닌지 판별
 *
 * */

@Component
public class NumericCheck {

    public boolean isNumeric(String str){
        for(char ch : str.toCharArray()){
            if(!Character.isDigit(ch)){
                return false;
            }
        }
        return true;
    }
}
