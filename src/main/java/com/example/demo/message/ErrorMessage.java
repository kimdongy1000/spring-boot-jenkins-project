package com.example.demo.message;

import lombok.Data;

@Data
public class ErrorMessage {

    private String errorMessage;

    private Throwable throwinge;

    private int httpStatus;


}
