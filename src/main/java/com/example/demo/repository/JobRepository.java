package com.example.demo.repository;

import com.example.demo.entity.JenkinsJobEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Repository
public interface JobRepository extends JpaRepository<JenkinsJobEntity, String> {


    Optional<JenkinsJobEntity> findByJobName(String jobName);
}
