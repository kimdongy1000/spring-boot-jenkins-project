package com.example.demo.repository;

import com.example.demo.compositkey.JenkinsLogCompositeKey;
import com.example.demo.entity.JenkinsLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends JpaRepository<JenkinsLogEntity , JenkinsLogCompositeKey> {
}
