package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * jenkins job 을 만드는 테이블
 *
 * JPA 에서 테이블은 @Entity 를 사용합니다 그래서 반드시 @Entity 를 포함
 * Table 는 이 테이블의 이름을 지정할 수 있습니다
 * Jpa 는 기본적으로 아무것도 없는 생성자를 필요합니다
 * 그외 저는 값을 편하게 입력 하기 위해서 @Builder , @Data , @AllArgsConstructor 사용했습니다
 *
 * */

@Entity
@Table(name = "JenkinsJobEntity")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JenkinsJobEntity {


    /**
     *  @Id 는 이 Key 가 이 엔티티의 기본키 (primaryKey 를 나타냅니다)
     *  @GeneratedValue 는 key 의 생성전략을 나타내는데 생성전략은 strategy 로 표현 이때 Auto 는 적절한 생성방식을 만들어내지만
     *  그외 다른 방식은 하단에 @SequenceGenerator 를 통해서 생성전략을 나타내주셔야 합니다
     *  @SequenceGenerator 시퀀스의 생성전략을 나타냅니다 name 은 시퀀스 생성기의 이름을 지정
     *                     sequenceName 은 데이터베이스가 사용할 이름입니다
     *                     initialValue 는 기본값이 1로 처음에 시작할 값을 입력할 수 있으며
     *                     allocationSize 한번에 할당하는 범위의 크기입니다
     *
     * */
    /*
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jon_sequence")
    @SequenceGenerator(name = "jon_sequence", sequenceName = "jon_sequence", initialValue = 1 ,allocationSize = 1)
    private Long id;
    */


    @Column(name = "job_name" , unique = true)
    @Id
    private String jobName;

    @Column(name = "insert_dts")
    private Date insertDts;

    @Column(name = "update_dts")
    private Date updateDts;
}
