package com.example.demo.entity;

import com.example.demo.compositkey.JenkinsLogCompositeKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "jenkinsLogEntity")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JenkinsLogEntity {

    @EmbeddedId
    private JenkinsLogCompositeKey id;

    @Column(name = "build_result")
    private char buildResult;

    @Column(name = "build_log" , length = 50000)
    private String buildLog;

    @Column(name = "insert_dts")
    private Date insertDts;
}
