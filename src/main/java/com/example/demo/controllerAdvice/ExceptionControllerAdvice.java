package com.example.demo.controllerAdvice;

import com.example.demo.message.ErrorMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 모든 핸들러 에러시 이곳에서 처리합니다
 *
 *
 *
 * */

@ControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<?> handleException(Exception e)
    {

        try{
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.setErrorMessage(e.getMessage());
            errorMessage.setThrowinge(e.getCause());
            errorMessage.setHttpStatus(500);

            ObjectMapper objectMapper = new ObjectMapper();
            String errorJson = objectMapper.writeValueAsString(errorMessage);

            return new ResponseEntity<>(errorJson , HttpStatus.INTERNAL_SERVER_ERROR);
        }catch (Exception e1){
            throw new RuntimeException(e1);
        }



    }
}
