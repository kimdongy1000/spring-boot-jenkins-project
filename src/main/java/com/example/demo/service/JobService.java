package com.example.demo.service;

import com.example.demo.common.NumericCheck;
import com.example.demo.compositkey.JenkinsLogCompositeKey;
import com.example.demo.config.JenkinsConfig;
import com.example.demo.dto.JenkinsJobDto;
import com.example.demo.entity.JenkinsJobEntity;
import com.example.demo.entity.JenkinsLogEntity;
import com.example.demo.repository.JobRepository;
import com.example.demo.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class JobService {


    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private JenkinsConfig jenkinsConfig;

    @Autowired
    private NumericCheck numericCheck;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private String returnOsSystemName;










    /**
    * 지정된 디렉터리에서 job 을 전부 영속성 컨텐츠로 등록
    *
    * */
    @Transactional(rollbackFor = Exception.class)
    public int mergeJobAll()
    {

        try{

            /**
             *
             * 읽어올 filePath 위치 지정
             *
             * */
            Path dir_path = Paths.get(jenkinsConfig.getJenkinsJobs());

            DirectoryStream<Path> stream = Files.newDirectoryStream(dir_path);

            for (Path file : stream){


                System.out.println("File: " + file);
                System.out.println("FileName: " + file.getFileName());

                JenkinsJobEntity jenkinsJobEntity = JenkinsJobEntity.builder()
                        .jobName(file.getFileName().toString())
                        .insertDts(new Date())
                        .updateDts(new Date())
                        .build();

                entityManager.merge(jenkinsJobEntity);

            }

            List<JenkinsJobEntity> jenkinsJobEntities =  jobRepository.findAll();
            for (JenkinsJobEntity list: jenkinsJobEntities) {

                System.out.println("name : " + list.getJobName());
                System.out.println("insertDate : " + list.getInsertDts());
            }

            return jenkinsJobEntities.size();

        }catch (Exception e){
            throw new RuntimeException(e);
        }


    }

    /**
     * 로그테이블을 전부 찾아서 로그자체를 저장
     *
     *
     * */
    @Transactional(rollbackFor = Exception.class)
    public int mergeJobLogAll() {

        int resultCount = 0;
        try{

            //List<JenkinsJobEntity> jenkinsJobEntities =  jobRepository.findAll();
            Optional<JenkinsJobEntity> jenkinsJobEntityOptional = jobRepository.findByJobName("jenkins_module1");

            JenkinsJobEntity jenkinsJobEntity = jenkinsJobEntityOptional.get();

            List<JenkinsJobEntity> jenkinsJobEntities = new ArrayList<>();

            jenkinsJobEntities.add(jenkinsJobEntity);


            for (JenkinsJobEntity item: jenkinsJobEntities) {

                StringBuffer jobPath = new StringBuffer();

                /**
                 * log 를 추출할 위치는 /var/lib/jenkins/jobs/[job이름]/build 기본위치
                 *
                 * */

                /**
                 * OS 사용으로 분기
                 *
                 * */
                if("windows".equals(returnOsSystemName)){

                    jobPath.append(jenkinsConfig.getJenkinsJobs());
                    jobPath.append("\\");
                    jobPath.append(item.getJobName());
                    jobPath.append("\\");
                    jobPath.append("builds");


                }

                //path for linux
                else if("linux".equals(returnOsSystemName)){


                    jobPath.append(jenkinsConfig.getJenkinsJobs());
                    jobPath.append("/");
                    jobPath.append(item.getJobName());
                    jobPath.append("/");
                    jobPath.append("builds");
                }





                Path jobLogPath = Paths.get(jobPath.toString());


                DirectoryStream<Path> stream = Files.newDirectoryStream(jobLogPath);


                for (Path file : stream){

                    JenkinsLogEntity jenkinsLogEntity = new JenkinsLogEntity();

                    System.out.println("FileName: " + file.getFileName());

                    if(numericCheck.isNumeric(file.getFileName().toString())){

                        /**
                         * 복합키 생성 생성 큐칙은 빌드 카운트 , job 이름
                         *
                         * */
                        JenkinsLogCompositeKey jenkinsLogCompositeKey = new JenkinsLogCompositeKey(Long.parseLong(file.getFileName().toString()) , item.getJobName());

                        /**
                         *
                         * 현재 최종 수정 시간 알아오기
                         * */
                        File BuildDir = file.toFile();
                        Long last_modify_long =  BuildDir.lastModified();
                        Date date = new Date(last_modify_long);

                        jenkinsLogEntity.setId(jenkinsLogCompositeKey);
                        jenkinsJobEntity.setInsertDts(date);

                        logRepository.save(jenkinsLogEntity);





                    }

                }

            }

            /*
            * C:\webProject\jenkins\jobs\jenkins_module1\builds\1 로그위치
            * jenkins.home.job=C:\\webProject\\jenkins\\jobs
            *
            *
            * */
            List<JenkinsLogEntity> jenkinsLogEntities = logRepository.findAll();

            String jenkins_job_path = jenkinsConfig.getJenkinsJobs();

            FileInputStream fis = null;

            BufferedReader reader = null;



            for (JenkinsLogEntity item : jenkinsLogEntities) {

                JenkinsLogCompositeKey item_composite_key = item.getId();
                String job_name = item_composite_key.getJobName();
                Long build_number = item_composite_key.getBuildNumber();

                /**
                 *
                 *
                 * */

                String log_path = null;

                //path for window
                if("windows".equals(returnOsSystemName)){

                    log_path = jenkins_job_path + "\\" + job_name +  "\\" + "builds" + "\\" + build_number + "\\" + "log";

                }

                //path for linux
                else if("linux".equals(returnOsSystemName)){
                    log_path = jenkins_job_path + "/" + job_name +  "/" + "builds" + "/" + build_number + "/" + "log";
                }




                fis = new FileInputStream(new File(log_path));
                reader = new BufferedReader(new InputStreamReader(fis));

                String line = null;

                StringBuffer job_log = new StringBuffer();
                while ((line = reader.readLine()) != null) {
                    job_log.append(line);
                    job_log.append("\n");

                }

                String full_log_str = job_log.toString().trim();
                String[] split_log = full_log_str.split("\n");


                /*
                * 빌드의 성공 여부는 Finished: FAILURE , Finished: SUCCESS 둘중 하나입니다
                *
                * */
                char buildResult = 'Y' ;

                if ("FAILURE".equals(split_log[split_log.length -1].split(":")[1].trim())){
                    buildResult = 'N';

                }else{
                    buildResult = 'Y';
                }

                item.setBuildResult(buildResult);
                item.setBuildLog(job_log.toString());
                item.setInsertDts(new Date());

                logRepository.save(item);

                resultCount = logRepository.findAll().size();



            }


        }catch(Exception e){
            throw  new RuntimeException(e);
        }

        return resultCount;


    }

    public List<JenkinsJobDto> getAllJobList()
    {

        List<JenkinsJobEntity> jenkinsJobEntities = jobRepository.findAll();



        return jenkinsJobEntities.stream().map(x -> {
            return new JenkinsJobDto(x.getJobName() , x.getInsertDts() , x.getUpdateDts());
        }).collect(Collectors.toList());

    }
}
