package com.example.demo.compositkey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


/**
 * jenkins 로그 테이블은 복합기 사용이 필요합니다 빌드 넘버링과 , job 이름이 pk 입니다
 *
 *
 * */

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class JenkinsLogCompositeKey implements Serializable {

    @Column(name = "build_number")
    private Long buildNumber;

    @Column(name = "job_name")
    private String jobName;



    @Override
    public String toString() {
        return "JenkinsLogCompositeKey{" +
                "buildNumber=" + buildNumber +
                ", jobName='" + jobName + '\'' +
                '}';
    }
}
