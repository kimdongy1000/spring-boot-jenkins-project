package com.example.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class TestController {

    @GetMapping("/testHandler")
    public String testHandler(
            @RequestParam("param1") String param1 ,
            @RequestParam("param2") String param2
            )
    {
        try{

            Map<String , Object> returnJson = new HashMap<>();
            returnJson.put("param1" , param1);
            returnJson.put("param2" , param2);

            ObjectMapper objectMapper = new ObjectMapper();

            String returnJsonMapper = objectMapper.writeValueAsString(returnJson);

            return returnJsonMapper;

        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
}
