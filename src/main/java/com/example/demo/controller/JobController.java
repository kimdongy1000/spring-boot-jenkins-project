package com.example.demo.controller;

import com.example.demo.dto.JenkinsJobDto;
import com.example.demo.service.JobService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("job")
public class JobController {

    @Autowired
    private JobService jobService;

    @Autowired
    private ObjectMapper objectMapper;


    @PostMapping("/mergeJob")
    @ResponseBody
    public ResponseEntity<?> mergeJobAll()
    {
        try{
           int merge_jobList =  jobService.mergeJobAll();

            Map<String , Object> resultMapper = new HashMap<>();

            resultMapper.put("successCount" , merge_jobList);
            String resultJsonString = objectMapper.writeValueAsString(resultMapper);


            return new ResponseEntity<>(resultJsonString , null , HttpStatus.OK );
        }catch(Exception e){
            throw new RuntimeException(e);
        }

    }

    @PostMapping("/mergeLog")
    @ResponseBody
    public ResponseEntity<?> mergeLog()
    {
        try{

            int mergeLogList = jobService.mergeJobLogAll();

            Map<String , Object> resultMapper = new HashMap<>();
            resultMapper.put("successCount" , mergeLogList);
            String resultJsonString = objectMapper.writeValueAsString(resultMapper);

            return new ResponseEntity<>(resultJsonString , HttpStatus.OK);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/getAllJob")
    public ResponseEntity<List<JenkinsJobDto>> getAllJobList()
    {

        try{

            List<JenkinsJobDto> returnList = jobService.getAllJobList();

            return new ResponseEntity<>(returnList , HttpStatus.OK);

        }catch(Exception e){
            throw new RuntimeException(e);
        }



    }

}
