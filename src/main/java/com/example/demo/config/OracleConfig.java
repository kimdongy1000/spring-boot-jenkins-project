package com.example.demo.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

@Configuration
@PropertySource("db.properties")
public class OracleConfig {

    @Value("${oracle.datasource.url}")
    private String url;

    @Value("${oracle.datasource.username}")
    private String username;

    @Value("${oracle.datasource.password}")
    private String password;

    @Bean
    public HikariConfig oracleHikariConfig(){

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);

        return hikariConfig;

    }

    @Bean
    public DataSource oracleDataSource(HikariConfig oracleHikariConfig){
        DataSource oracleDataSource = new HikariDataSource(oracleHikariConfig);
        return oracleDataSource;
    }

    @Bean
    public SqlSessionFactory oracleSqlSessionFactory(DataSource oracleDataSource) throws Exception{

        SqlSessionFactoryBean oracleSqlSessionFactoryBean = new SqlSessionFactoryBean();
        oracleSqlSessionFactoryBean.setDataSource(oracleDataSource);
        //oracleSqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/static/mapper/multiple/oracle/*.xml"));

        return oracleSqlSessionFactoryBean.getObject();

    }

    @Bean
    public SqlSessionTemplate oracleSqlSessionTemplate(SqlSessionFactory oracleSessionFactory) throws Exception{
        SqlSessionTemplate oracleSqlSessionTemplate = new SqlSessionTemplate(oracleSessionFactory);
        return oracleSqlSessionTemplate;
    }


}
