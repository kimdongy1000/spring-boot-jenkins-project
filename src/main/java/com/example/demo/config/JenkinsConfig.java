package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@PropertySource("classpath:jenkins.properties")
@Component
public class JenkinsConfig {

    @Value("${jenkins.home}")
    private String jenkinsHome;

    @Value("${jenkins.home.job}")
    private String jenkinsJobs;

    public String getJenkinsHome() {
        return jenkinsHome;
    }

    public String getJenkinsJobs() {
        return jenkinsJobs;
    }
}
